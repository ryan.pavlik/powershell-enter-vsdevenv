# Copyright 2022, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0

# Original Author: Ryan Pavlik <ryan.pavlik@collabora.com>

# Maintained at https://gitlab.freedesktop.org/ryan.pavlik/powershell-enter-vsdevenv

# Wrapper for the Microsoft.VisualStudio.DevShell module and its contained cmdlet Enter-VsDevShell,
# which uses vswhere to find everything for you.
function Enter-VsDevEnv {
    param (
        $Version = 17,
        $Year,
        $Arch = "amd64",
        $HostArch = "amd64"
    )
    if ($Year) {
        switch ($Year) {
            2022 { $Version = 17 }
            2019 { $Version = 16 }
            2017 { $Version = 15 }
            Default {
                Write-Error "Did not recognize VS release year $Year"
                throw "Bad year $Year"
            }
        }
    }
    $VswhereCmd = Get-Command vswhere -ErrorAction SilentlyContinue
    if ($VswhereCmd) {
        $Vswhere = $VswhereCmd.Source
    } else {
        $Vswhere = "${env:ProgramFiles(x86)}\Microsoft Visual Studio\Installer\vswhere.exe"
    }

    if (!(Test-Path $Vswhere)) {
        Write-Error "Could not find vswhere on path or from VS installer! Are you sure you have VS installed? Last try was $Vswhere"
        throw "Missing vswhere"
    }

    $InstallPath = & "$Vswhere" -version $Version -property installationpath -latest
    if (!$InstallPath) {
        # Try again with all products, to pick up build tools.
        $InstallPath = & "$Vswhere" -version $Version -products * -property installationpath -latest
    }
    if (!$InstallPath) {
        Write-Error "Could not find an install of Visual Studio $Version!"
        throw "Missing VS install"
    }
    Write-Output "Will use Visual Studio $Version at: $installPath"

    $Module = & "$Vswhere" -version $Version -products * -latest -find "**\Microsoft.VisualStudio.DevShell.dll"

    if (!$Module) {
        $Module = & "$Vswhere" -products * -latest -find "**\Microsoft.VisualStudio.DevShell.dll"
    }

    if (!$Module) {
        Write-Error "Could not find any version of Microsoft.VisualStudio.DevShell.dll!"
        throw "Missing Microsoft.VisualStudio.DevShell.dll"
    }

    Import-Module $Module
    Enter-VsDevShell -VsInstallPath $InstallPath -SkipAutomaticLocation -DevCmdArguments "-no_logo -arch=$Arch -host_arch=$HostArch"
}
